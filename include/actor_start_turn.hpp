// =============================================================================
// Copyright 2011-2022 Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef ACTOR_START_TURN_HPP
#define ACTOR_START_TURN_HPP

namespace actor
{
class Actor;

void start_turn(Actor& actor);

}  // namespace actor

#endif  // ACTOR_START_TURN_HPP
