// =============================================================================
// Copyright 2011-2022 Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef BOT_HPP
#define BOT_HPP

namespace bot
{
void init();

void act();

}  // namespace bot

#endif  // BOT_HPP
